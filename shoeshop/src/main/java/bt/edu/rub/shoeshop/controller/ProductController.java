package bt.edu.rub.shoeshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import bt.edu.rub.shoeshop.entity.Product;
import bt.edu.rub.shoeshop.service.ProductService;
import bt.edu.rub.shoeshop.service.ImageUploadService;

import org.springframework.web.multipart.MultipartFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    private final ProductService productService;
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    private ImageUploadService imageUploadService;

    @Autowired
    public ProductController(ProductService productService, ImageUploadService imageUploadService) {
        this.productService = productService;
        this.imageUploadService = imageUploadService;
    }

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts() {
        return ResponseEntity.ok(productService.getAllProducts());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable String id) {
        return productService.getProductById(id)
                .map(product -> ResponseEntity.ok(product))
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    // @PutMapping("/{id}")
    // public ResponseEntity<Product> updateProduct(@PathVariable String id,
    // @RequestBody Product product) {
    // if (!productService.getProductById(id).isPresent()) {
    // return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    // }

    // product.setId(id);
    // Product updatedProduct = productService.updateProduct(product);
    // return ResponseEntity.ok(updatedProduct);
    // }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable String id) {
    if (!productService.getProductById(id).isPresent()) {
    return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    productService.deleteProduct(id);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping(value = "/{id}", consumes = "multipart/form-data")
    public ResponseEntity<Product> updateProductWithPhoto(
            @PathVariable String id,
            @RequestPart("productName") String productName,
            @RequestPart("productDescription") String productDescription,
            @RequestPart("productPrice") String productPrice,
            @RequestPart("category") String category,
            @RequestPart(value = "photo", required = false) MultipartFile photo) {
        try {
            Optional<Product> existingProductOpt = productService.getProductById(id);
            if (!existingProductOpt.isPresent()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }

            Product existingProduct = existingProductOpt.get();
            existingProduct.setProductName(productName);
            existingProduct.setProductDescription(productDescription);
            existingProduct.setProductPrice(productPrice);
            existingProduct.setCategory(category);

            if (photo != null && !photo.isEmpty()) {
                // Upload the new image and get its URL
                String productImgUrl = imageUploadService.uploadImage(photo);
                existingProduct.setPhoto(productImgUrl);
            }

            // Update the product with the new details
            Product updatedProduct = productService.save(existingProduct);

            return ResponseEntity.ok(updatedProduct);
        } catch (Exception e) {
            // Handle the exception appropriately
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PostMapping(consumes = "multipart/form-data")
    public ResponseEntity<Product> createProductWithPhoto(
            @RequestPart("productName") String productName,
            @RequestPart("productDescription") String productDescription,
            @RequestPart("productPrice") String productPrice,
            @RequestPart("category") String category,
            @RequestPart("photo") MultipartFile photo) {
        try {
            // Create a new Product instance and set its fields
            Product product = new Product();
            product.setProductName(productName);
            product.setProductDescription(productDescription);
            product.setProductPrice(productPrice);
            product.setCategory(category);

            // Save the product initially to get the ID
            Product savedProduct = productService.save(product);

            // Upload the image and get its URL
            String productImgUrl = imageUploadService.uploadImage(photo);

            // Set the photo URL in the product
            savedProduct.setPhoto(productImgUrl);

            // Update the product with the photo URL
            productService.uploadProductPhoto(savedProduct.getId(), photo);

            // Save the product again with the updated photo URL
            productService.save(savedProduct);

            return ResponseEntity.status(HttpStatus.CREATED).body(savedProduct);
        } catch (Exception e) {
            // Handle the exception appropriately
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

}