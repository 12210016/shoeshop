package bt.edu.rub.shoeshop.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import jakarta.validation.constraints.Size;

@Document(collection = "product") // This indicates it's a MongoDB document
public class Product {
    @Id // Use this to designate the primary key in MongoDB
    private String id;

    @Field("product_name") // Specifies the field name in MongoDB
    private String productName;

    @Field("product_description")
    private String productDescription;

    @Field
    @Size(min = 10, max = 500)
    private String photo;

    @Field("product_price")
    private String productPrice;

    @Field("Category")
    private String category;

    // Default constructor
    public Product() {
    }

    // Constructor with arguments
    public Product(String productName, String productDescription, String photo, String productPrice) {
        this.productName = productName;
        this.productDescription = productDescription;
        this.photo = photo;
        this.productPrice = productPrice;
    }

    // Getters and Setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }
}