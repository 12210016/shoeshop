package bt.edu.rub.shoeshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.transaction.annotation.Transactional;

import bt.edu.rub.shoeshop.entity.Product;
import bt.edu.rub.shoeshop.repository.ProductRepository;
import bt.edu.rub.shoeshop.exception.FileSizeException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import javax.management.RuntimeErrorException;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final String uploadDir = "images"; // Consider external storage for production

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Optional<Product> getProductById(String id) {
        return productRepository.findById(id);
    }

    @Override
    public Product findByID(String id) {
        return getProductById(id)
                .orElseThrow(() -> new RuntimeErrorException(null, "Product with ID " + id + " not found"));
    }

    @Override
    public Product updateProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void deleteProduct(String id) {
        productRepository.deleteById(id);
    }

    @Transactional
    @Override
    public void uploadProductPhoto(String id, MultipartFile photo) throws IOException {
        Product product = findByID(id);

        if (photo.getSize() > 1024 * 1024) {
            throw new FileSizeException("File size must be less than 1MB");
        }

        String originalFilename = StringUtils.cleanPath(photo.getOriginalFilename());
        String filenameExtension = originalFilename.substring(originalFilename.lastIndexOf(".") + 1).toLowerCase();

        if (!List.of("jpg", "jpeg", "png").contains(filenameExtension)) {
            throw new IllegalArgumentException("Invalid file extension");
        }

        String filenameWithoutExtension = originalFilename.substring(0, originalFilename.lastIndexOf("."));
        String timestamp = String.valueOf(System.currentTimeMillis());
        String filename = filenameWithoutExtension + "_" + timestamp + "." + filenameExtension;

        Path uploadPath = Paths.get(uploadDir, filename);
        Files.createDirectories(uploadPath.getParent()); // Ensure the directory exists

        photo.transferTo(uploadPath); // Save the uploaded photo

        product.setPhoto(filename); // Store the photo filename in the product entity
        productRepository.save(product);
    }
}
