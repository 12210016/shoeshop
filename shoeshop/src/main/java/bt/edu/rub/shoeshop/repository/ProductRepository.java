package bt.edu.rub.shoeshop.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import bt.edu.rub.shoeshop.entity.Product;

public interface ProductRepository extends MongoRepository<Product, String> {
}