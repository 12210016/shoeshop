package bt.edu.rub.shoeshop.service;

import bt.edu.rub.shoeshop.entity.Product;

import java.util.List;
import java.util.Optional;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;

public interface ProductService {

    List<Product> getAllProducts();

    Optional<Product> getProductById(String id);

    Product updateProduct(Product product);

    void deleteProduct(String id);

    void uploadProductPhoto(String id, MultipartFile photo) throws IOException;

    Product findByID(String theId);

    Product save(Product product);
}
